#!/usr/bin/env python3

import pwn

hex_list = [0x7d, 0x6e75665f, 0x73695f65, 0x73726576, 0x65725f7b, 0x47414c46]

flag = ""
flag = "".join(pwn.p32(hex_list[num]).decode() for num in range(5, -1, -1))

print(flag)
