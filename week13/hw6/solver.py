#!/usr/bin/env python3

import time
import subprocess


def cnt_int(target):
    cmd = "./pin -t ./inscount0.so -- ./break".split()

    with subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE) as p:
        p.communicate(input=target.encode())


def get_res():
    with open("./inscount.out", "r") as f:
        ins_cnt = f.readline().split()[1]

    return int(ins_cnt)


def guess_nxt_char(prefix):
    
    char_idx = 32
    ins_rcd = [0] * 2
    len_prefix = len(prefix)

    # guess next char by checking instr count within while loop
    while (len_prefix == len(prefix)):
        assert(32 <= char_idx <= 126)
        cnt_int(prefix + chr(char_idx))
        res = get_res()

        # the first check (for space)
        if (ins_rcd[0] == 0):
            ins_rcd[0] = char_idx
            ins_rcd[1] = res
            print(res)
        # none of current and previous word is correct
        elif (-30 < ins_rcd[1] - res < 30):
            ins_rcd[0] = char_idx
            char_idx += 1
        # one of the first (space) or second (exclamation) char is the next char
        elif (ins_rcd[0] == ord(' ')):
            cnt_int(prefix + chr(char_idx + 1))
            prefix += "!" if ins_rcd[0] == res else " "
        # ins cnt of cur char is different from more than 1 of other tested char
        else:
            prefix += chr(char_idx)

    ins_rcd[1] = res
    print(ins_rcd)
    print(prefix)
    return prefix



def main(): 
    target = ""
    target = guess_nxt_char(target)

    while (target[-1] != '}'):
        target = guess_nxt_char(target)

    print(target)


if __name__ == "__main__":
    main()
