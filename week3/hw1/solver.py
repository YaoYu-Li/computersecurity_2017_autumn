#!/usr/bin/env python3

import sys


def get_dword_bytes(cipher, bit_index):
    dword = cipher[bit_index * 4]
    dword += cipher[bit_index * 4 + 1]
    dword += cipher[bit_index * 4 + 2]
    dword += cipher[bit_index * 4 + 3]

    return dword
    

def reverse_dec_from_bytes(cipher):
    cipher_dec = []

    # transmit bytes stream to integer
    for i in range(0, len(cipher), 4):
        cipher_dec.append(int.from_bytes(cipher[i : i + 4], byteorder = "little"))

    return cipher_dec
    

def get_enc_iteration(cipher_len):
    iteration = [0] * cipher_len
    for i in range(cipher_len):
        iteration[i] = (i + 1) << ((i + 2) % 10)

    return iteration


def decrypt(cipher):
    plaintext = [0] * (len(cipher) // 4)

    # change the bytes array to 4 bytes integer array
    cipher_dec = reverse_dec_from_bytes(cipher)

    # get the iteration value for each round
    enc_iteration = get_enc_iteration(len(cipher_dec))

    # decrypt the value from integer
    for i in range(len(cipher_dec)):
        plaintext[i] = (cipher_dec[i] - 0x2333) // enc_iteration[i]

    return plaintext



def main():
    if len(sys.argv) != 2:  
        print("Usage: ./solver.py <encrypted flag>")
        sys.exit()

    with open(sys.argv[1], 'rb') as f:
        cipher = f.read()

    plaintext = decrypt(cipher)

    flag = ""
    for i in range(len(plaintext)):
        flag += chr(plaintext[i])

    print(flag)


if __name__ == "__main__":
    main()
