#include <stdio.h>
#include <string.h>

void encrypt(char *plaintext, size_t plaintext_len)
{
    FILE *fptr;
    fptr = fopen("flag", "wb");

    if (plaintext_len != 0) {
        unsigned int it; /* iteration value to change the iv */
        unsigned int cipher[plaintext_len];
        for (unsigned int i = 0; i < plaintext_len; i++) {
            it = (i + 1) << ((i + 2) % 10);
            cipher[i] = plaintext[i] * it + (unsigned int)0x2333;

            fwrite(&cipher[i], 4, 1, fptr);
        }
    }

    fclose(fptr);
}

int main()
{
    char input[60];

    memset(input, 0, sizeof(input));
    scanf("%60s", input);

    encrypt(input, strlen(input));

    return 0;
}
