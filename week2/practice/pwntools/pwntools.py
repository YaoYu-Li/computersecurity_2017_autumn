#!/usr/bin/env python3

import pwn
import sys

value = 0


def help_binary_search(hint, ubound, lbound): # range is from lbound to ubound
    global value 

    if hint[-2] == 'l': # input value is too small
        lbound = value
    elif hint[-2] == 'g': # input value is too big
        ubound = value
    else:
        print(hint)
        sys.exit()

    value = int((lbound + ubound) / 2)
    return [ubound, lbound]


def do_binary_search(remote, ubound, lbound):
    global value 

    while True:
        remote.sendline(str(value).encode())
        ret = remote.recvline().decode()
        [ubound, lbound] = help_binary_search(ret, ubound, lbound)


def main():
    global value 

    ubound = 50000000
    lbound = 1

    value = int((lbound + ubound) / 2)

    r = pwn.remote("csie.ctf.tw", 10123)
    ret = r.recvline().decode()


    do_binary_search(r, ubound, lbound)


if __name__ == "__main__":
    main()
